import sys

class Chip():
    def __init__(self, info):
        self.name = info[0]
        self.posX = float(info[1])    # micro
        self.posY = float(info[2])    # micro
        self.pxSize = float(info[3])
        self.pxNumberX = float(info[4])
        self.pxNumberY = float(info[5])
        self.dev_type = info[6]
        self.readtime = float(info[7])
        self.group = info[8]
        self.euler01 =float(info[9])  # degree
        self.euler02 =float(info[10])
        self.euler03 =float(info[11])
        self.translations01 = float(info[12])  #mm
        self.translations02 = float(info[13])
        self.translations03 = float(info[14])
        self.deform_type = info[15]
        self.deform_coeff = float(info[16])
        self.QE_variation = float(info[17])
        self.others = info[18:]

    def printChip(self):
        A = self.name +"\t"+ str(self.posX)+"\t"+str(self.posY) + "\t" + str(self.pxSize) +"\t"
        B = str(self.pxNumberX) + "\t" +str(self.pxNumberY) +"\t" +self.dev_type + "\t"
        C = str(self.readtime)+ "\t" +self.group +"\t"
        D = str(self.euler01) + "\t" + str(self.euler02) + "\t" +str(self.euler03) + "\t"
        E = str(self.translations01) + "\t" + str(self.translations02) + "\t" +str(self.translations03) + "\t"
        F = self.deform_type + "\t" + str(self.deform_coeff) + "\t" + str(self.QE_variation) +"\t"
        for item in self.others:
             F = F + item +"\t"
        return A+B+C+D+E+F+"\n"


def generateChipDict(input):
    chipDict ={"header":[]}
    ifile = open(input,'rU')

    for line in ifile.readlines(): 
        if line[0]=='#':
            chipDict["header"].append(line)
        else:
            info = line.split()
            chip = Chip(info)
            chipDict[chip.name]=chip
    return chipDict

def writeOutChipDict(chipDict):
    ofile = open("focalplanelayout.txt",'w')
    for item in chipDict["header"]:
        ofile.write(item)
    for key in sorted(chipDict):
        if key!="header":
            ofile.write(chipDict[key].printChip())
    ofile.close()

def update(chipDict):
    for key in sorted(chipDict):
        if key=="header" or 'C' in key:
            continue
        else:
            chipDict[key].pxSize=1.0
            chipDict[key].pxNumberX *=10
            chipDict[key].pxNumberY *=10
    return chipDict

if __name__=="__main__":
    chipDict = generateChipDict("focalplanelayout_default.txt")
    new_chipDict = update(chipDict)
    writeOutChipDict(new_chipDict)



